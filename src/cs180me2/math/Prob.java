package cs180me2.math;

import cs180me2.index.hashmap.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Prob {
    private static TopicIndex tidx;
    
    public static Double getPP(int documentsInClass, int totalDocuments) {
        return (double)documentsInClass / (double)totalDocuments;
    }
    
    public static Double getPPLog(int documentsInClass, int totalDocuments) {
        return Math.log(getPP(documentsInClass, totalDocuments));
    }
    
    public static Double getCP(int instancesOfWord, int instancesOfClass, double lambda) {
        tidx = TopicIndex.getInstance();
        double nct = (double)instancesOfWord;
        double nc = (double)instancesOfClass;        
        return (nct + lambda) / (nc + lambda * tidx.size()) ;
    }
    
    public static Double getCPC(int instancesOfWord, int instancesOfClass, double lambda) {
        return 1.0 - getCP(instancesOfWord, instancesOfClass, lambda);
    }

    public static Double getCPLog(int instancesOfWord, int instancesOfClass, double lambda) {
        return Math.log(getCP(instancesOfWord, instancesOfClass, lambda));
    }
    
    public static Double getCPCLog(int instancesOfWord, int instancesOfClass, double lambda) {
        return Math.log(1 - getCP(instancesOfWord, instancesOfClass, lambda));
    }
    
}
