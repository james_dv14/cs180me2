package cs180me2.math;

import cs180me2.index.IndexBase;
import cs180me2.index.array.AbsenceIndex;
import cs180me2.index.hashmap.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ProbArray {
    private static TopicIndex tidx;
    private static InstanceIndex iidx;
    private static AbsenceIndex aidx;
    
    public static Double[] getArray(String key) {
        tidx = TopicIndex.getInstance();
        try {
            iidx = InstanceIndex.getInstance();
            aidx = AbsenceIndex.getInstance();
        } 
        catch (IndexBase.IndexDependencyException ex) {
            System.exit(0);
        }
        
        Double[] arr = new Double[tidx.size()];
        
        for (String topic : tidx.keys()) {
            int nct = iidx.getInstances(key, topic);
            int nt = tidx.getInstances(topic);
            double lambda = 0.01;
            
            Double presenceProb = Prob.getCPLog(nct, nt, lambda);
            Double absenceProb = Prob.getCPCLog(nct, nt, lambda);
            arr[tidx.getIndex(topic)] = presenceProb - absenceProb;
            aidx.add(topic, absenceProb);
        }
        
        return arr;
    }

}
