package cs180me2.math;

import cs180me2.index.IndexBase;
import cs180me2.index.array.*;
import cs180me2.index.hashmap.PresenceIndex;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class NaiveBayes {

    public static double getCPLog(String topic, HashSet<String> wordsInDoc) throws IndexBase.IndexDependencyException { 
        PriorIndex pridx = PriorIndex.getInstance();
        PresenceIndex pidx = PresenceIndex.getInstance();
        AbsenceIndex aidx = AbsenceIndex.getInstance();
        
        double score = pridx.getPriorProbability(topic);
        score += aidx.getTotalAbsenceProbability(topic);
        for (String word : wordsInDoc)
            score += pidx.getProbability(word, topic);
        
        return score;
    }
    
}
