package cs180me2.math;

import cs180me2.index.hashmap.TopicIndex;
import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ProbApfloat {
    private static TopicIndex tidx;
    
    public static Apfloat getCP(int instancesOfWord, int instancesOfClass, double lambda) {
        tidx = TopicIndex.getInstance();
        Apfloat nct = new Apfloat(instancesOfWord);
        Apfloat nc = new Apfloat(instancesOfClass);
        Apfloat lamb = new Apfloat(lambda);
        Apfloat topics = new Apfloat(tidx.size());
        return (nct.add(lamb)).divide(nc.add(lamb.multiply(topics)));
    }
    
    public static Apfloat getCPC(int instancesOfWord, int instancesOfClass, double lambda) {
        return (new Apfloat(1.0)).subtract(getCP(instancesOfWord, instancesOfClass, lambda));
    }

    public static Apfloat getCPLog(int instancesOfWord, int instancesOfClass, double lambda) {
        return ApfloatMath.log(getCP(instancesOfWord, instancesOfClass, lambda));
    }
    
    public static Apfloat getCPCLog(int instancesOfWord, int instancesOfClass, double lambda) {
        return ApfloatMath.log(getCPC(instancesOfWord, instancesOfClass, lambda));
    }
    
}
