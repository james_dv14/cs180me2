package cs180me2.util.stem;

import cs180me2.index.hashset.Stopwords;
import org.tartarus.snowball.ext.englishStemmer;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Stemmer {
    public static String getStem(String word) {   
        Stopwords stop = Stopwords.getInstance();
        if (stop.contains(word)) return word;
        String word_lc = word.toLowerCase();
        englishStemmer porter2 = new englishStemmer();
        porter2.setCurrent(word_lc);
        String word_stem = porter2.stem() ? porter2.getCurrent() : word_lc;
        return stop.contains(word_stem) ? word_lc : word_stem;
    }
}
