package cs180me2.util.fileio;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T> Type of element.
 * @studentno 2012-78002
 */
public interface CollectionPrinter<T> {

    public void printElement(T e);
    
}
