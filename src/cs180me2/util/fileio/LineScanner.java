package cs180me2.util.fileio;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface LineScanner {

    public void processLine(String line);
    
}
