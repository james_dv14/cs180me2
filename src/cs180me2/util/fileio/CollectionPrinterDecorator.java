package cs180me2.util.fileio;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Scanner;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T> Type of element.
 * @studentno 2012-78002
 */
public abstract class CollectionPrinterDecorator<T> implements CollectionPrinter {
    //***************************************
    // Objects
    //***************************************
    protected final String filepath;
    protected final Collection<T> collection;
    protected PrintWriter out;
    //***************************************
    // Constructor
    //***************************************
    public CollectionPrinterDecorator(String filepath, Collection<T> collection) {
        this.filepath = filepath;
        this.collection = collection;
    }
    //***************************************
    // Main method
    //***************************************
    public void print() {
        try {
            out = Output.getWriter(filepath);
            for (T e : collection)
                printElement(e); // abstract method to be decorated
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
