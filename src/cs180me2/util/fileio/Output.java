package cs180me2.util.fileio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Output {
    public static PrintWriter getWriter(String filepath) throws IOException {
        return new PrintWriter(new FileWriter(new File(filepath), false));
    }
}
