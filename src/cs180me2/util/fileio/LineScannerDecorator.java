package cs180me2.util.fileio;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class LineScannerDecorator implements LineScanner {
    //***************************************
    // Objects
    //***************************************
    protected final String filepath;
    private Scanner in;
    //***************************************
    // Constructor
    //***************************************
    public LineScannerDecorator(String filepath) {
        this.filepath = filepath;
    }
    //***************************************
    // Main method
    //***************************************
    public void scan() {
        try {
            in = Input.getScanner(filepath);
            while(in.hasNextLine()) {
                String line = in.nextLine();
                if(!line.isEmpty())
                    processLine(line); // abstract method to be decorated
            }
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
