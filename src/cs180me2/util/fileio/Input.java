package cs180me2.util.fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Input {
    public static Scanner getScanner(String filepath) throws FileNotFoundException {
        return new Scanner(new File(filepath));
    }
}
