package cs180me2.index;

import cs180me2.util.fileio.CollectionPrinterDecorator;


/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class IndexBase {

    public void print(CollectionPrinterDecorator cp) {
        cp.print();
    }
    
    public class IndexDependencyException extends Exception {
        public IndexDependencyException() {
        }
    }
}
