package cs180me2.index.hashset;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Stopwords extends HashSetIndex<String> {
    //***************************************
    // Singleton implementation
    //***************************************
    private static Stopwords instance;
    
    private Stopwords() {
        super();
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static Stopwords getInstance() {
        if (isNotInstantiated())
            instance = new Stopwords();
        return instance;
    } 
}