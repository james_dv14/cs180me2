package cs180me2.index.hashset;

import cs180me2.index.IndexBase;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T> Type of value stored in hash.
 * @studentno 2012-78002
 */
public class HashSetIndex<T> extends IndexBase {
    //***************************************
    // Index
    //***************************************
    protected HashSet index;
    //***************************************
    // Constructor
    //***************************************
    protected HashSetIndex() {
        index = new HashSet<T>();
    }
    //***************************************
    // HashSet accessors
    //***************************************
    public int size() {
        return index.size();
    }
    
    public boolean contains(T key) {
        return index.contains(key);
    }
    
    public void add(T key) {
        index.add(key);
    }
    
    public void remove(T key) {
        index.remove(key);
    }
    
    public void clear() {
        index.clear();
    }
 }