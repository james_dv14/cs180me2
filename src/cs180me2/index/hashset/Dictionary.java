package cs180me2.index.hashset;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Dictionary extends HashSetIndex<String> {
    //***************************************
    // Singleton implementation
    //***************************************
    private static Dictionary instance;
    
    private Dictionary() {
        super();
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static Dictionary getInstance() {
        if (isNotInstantiated())
            instance = new Dictionary();
        return instance;
    } 
}