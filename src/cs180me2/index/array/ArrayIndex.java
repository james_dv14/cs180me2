package cs180me2.index.array;

import cs180me2.index.IndexBase;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T> Type of value stored in array.
 * @studentno 2012-78002
 */
public class ArrayIndex<T> extends IndexBase {
    //***************************************
    // Index
    //***************************************
    protected T[] index;
    //***************************************
    // Array accessors
    //***************************************
    public int length() {
        return index.length;
    }
 }