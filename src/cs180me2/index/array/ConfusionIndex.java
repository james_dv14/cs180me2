package cs180me2.index.array;

import cs180me2.index.hashmap.TopicIndex;
import cs180me2.util.fileio.CollectionPrinterDecorator;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ConfusionIndex extends ArrayIndex<int[]> {
    //***************************************
    // Index dependencies
    //***************************************
    TopicIndex tidx;
    //***************************************
    // Singleton implementation
    //***************************************
    private static ConfusionIndex instance;
    
    private ConfusionIndex() throws IndexDependencyException {
        if (TopicIndex.isNotInstantiated())
            throw new IndexDependencyException();
        tidx = TopicIndex.getInstance();
        index = new int[tidx.size()][tidx.size()];
        // System.err.println(index.length + " x " + index[index.length-1].length);
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static ConfusionIndex getInstance() throws IndexDependencyException {
        if (isNotInstantiated())
            instance = new ConfusionIndex();
        return instance;
    } 
    //***************************************
    // Semantic alias for get
    //***************************************
    public int getClassifications(String expected, String result) {
        return index[tidx.getIndex(expected)][tidx.getIndex(result)];
    }
    //***************************************
    // Put
    //***************************************
    public void put(String expected, String result) {
        index[tidx.getIndex(expected)][tidx.getIndex(result)] += 1;
    }
    //***************************************
    // Accuracy
    //***************************************
    public String getAccuracy(double files) {
        DecimalFormat df = new DecimalFormat("##.##");
        df.setRoundingMode(RoundingMode.DOWN);
        int correct = 0;
        for (String topic : tidx.keys())
            correct += index[tidx.getIndex(topic)][tidx.getIndex(topic)];
        return df.format((double)correct / files * 100) + "%";
    }
    //***************************************
    // Print
    //***************************************
    public void print(String filepath) {
        super.print(new ConfusionIndexPrinter(filepath));
    }
    
    private class ConfusionIndexPrinter extends CollectionPrinterDecorator {
        //***************************************
        // Constructor
        //***************************************
        public ConfusionIndexPrinter(String filepath) {
            super(filepath, tidx.keys());
        }
        //***************************************
        // Decorate abstract method
        //***************************************
        @Override
        public void printElement(Object e) {
            out.print(e);
            out.print("|");
            for (String r : tidx.keys()) {
                out.print(getClassifications((String)e, r));
                if (((String)e).equals(r))
                    out.print("*");
                out.print("|");
            }
            out.println();
        }
    }
}