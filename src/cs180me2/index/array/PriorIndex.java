package cs180me2.index.array;

import cs180me2.index.hashmap.TopicIndex;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PriorIndex extends ArrayIndex<Double> {
    //***************************************
    // Index dependencies
    //***************************************
    TopicIndex tidx;
    //***************************************
    // Singleton implementation
    //***************************************
    private static PriorIndex instance;
    
    private PriorIndex() throws IndexDependencyException {
        if (TopicIndex.isNotInstantiated())
            throw new IndexDependencyException();
        tidx = TopicIndex.getInstance();
        index = new Double[tidx.size()];
        for (int i = 0; i < index.length; i++)
            index[i] = 0.0;
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static PriorIndex getInstance() throws IndexDependencyException {
        if (isNotInstantiated())
            instance = new PriorIndex();
        return instance;
    } 
    //***************************************
    // Semantic alias for get
    //***************************************
    public Double getPriorProbability(String topic) {
        return index[tidx.getIndex(topic)];
    }
    //***************************************
    // Put
    //***************************************
    public void set(String topic, double value) {
        index[tidx.getIndex(topic)] = value;
    }
}