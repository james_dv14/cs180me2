package cs180me2.index.array;

import cs180me2.index.hashmap.TopicIndex;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class AbsenceIndex extends ArrayIndex<Double> {
    //***************************************
    // Index dependencies
    //***************************************
    TopicIndex tidx;
    //***************************************
    // Singleton implementation
    //***************************************
    private static AbsenceIndex instance;
    
    private AbsenceIndex() throws IndexDependencyException {
        if (TopicIndex.isNotInstantiated())
            throw new IndexDependencyException();
        tidx = TopicIndex.getInstance();
        index = new Double[tidx.size()];
        for (int i = 0; i < index.length; i++)
            index[i] = 0.0;
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static AbsenceIndex getInstance() throws IndexDependencyException {
        if (isNotInstantiated())
            instance = new AbsenceIndex();
        return instance;
    } 
    //***************************************
    // Semantic alias for get
    //***************************************
    public Double getTotalAbsenceProbability(String topic) {
        return index[tidx.getIndex(topic)];
    }
    //***************************************
    // Put
    //***************************************
    public void add(String topic, double value) {
        index[tidx.getIndex(topic)] += value;
    }
    
    public void multiply(String topic, double value) {
        index[tidx.getIndex(topic)] *= value;
    }
}