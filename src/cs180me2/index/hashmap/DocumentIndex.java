package cs180me2.index.hashmap;

import cs180me2.util.fileio.CollectionPrinterDecorator;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class DocumentIndex extends HashMapIndex<String, String> {
    //***************************************
    // Singleton implementation
    //***************************************
    private static DocumentIndex instance;
    
    private DocumentIndex() {
        super();
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static DocumentIndex getInstance() {
        if (isNotInstantiated())
            instance = new DocumentIndex();
        return instance;
    } 
    //***************************************
    // Accessor - write
    //***************************************
    public void put(String filename, String topic) {
        index.put(filename, topic);
    }
    //***************************************
    // Accessor - read
    //***************************************
    public String getTopic(String filename) {
        return (String)index.get(filename);
    }
    //***************************************
    // Print
    //***************************************
    public void print(String filepath) {
        super.print(new DocumentIndexPrinter(filepath));
    }
    
    private class DocumentIndexPrinter extends CollectionPrinterDecorator {
        //***************************************
        // Constructor
        //***************************************
        public DocumentIndexPrinter(String filepath) {
            super(filepath, keys());
        }
        //***************************************
        // Decorate abstract method
        //***************************************
        @Override
        public void printElement(Object e) {
            out.print(e + " ");
            out.println(getTopic((String) e));
        }
    }
}