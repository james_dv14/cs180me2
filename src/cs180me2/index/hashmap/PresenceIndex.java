package cs180me2.index.hashmap;

import cs180me2.math.ProbArray;
import cs180me2.util.fileio.CollectionPrinterDecorator;


/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PresenceIndex extends PerTopicIndex<Double> {
    //***************************************
    // Singleton implementation
    //***************************************
    private static PresenceIndex instance;
    
    private PresenceIndex() throws IndexDependencyException {
        super();
        if (InstanceIndex.isNotInstantiated())
            throw new IndexDependencyException();
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static PresenceIndex getInstance() throws IndexDependencyException {
        if (isNotInstantiated())
            instance = new PresenceIndex();
        return instance;
    }
    //***************************************
    // Put
    //***************************************
    public void put(String key) {
        index.put(key, ProbArray.getArray(key));
    }
    //***************************************
    // Semantic alias for get
    //***************************************
    public Double getProbability(String word, String topic) {
        return super.get(word, topic);
    }
    //***************************************
    // Print
    //***************************************
    public void print(String filepath) {
        super.print(new PresenceIndexPrinter(filepath));
    }
    
    private class PresenceIndexPrinter extends CollectionPrinterDecorator {
        //***************************************
        // Constructor
        //***************************************
        public PresenceIndexPrinter(String filepath) {
            super(filepath, keys());
        }
        //***************************************
        // Decorate abstract method
        //***************************************
        @Override
        public void printElement(Object e) {
            out.println(e);
            for (String topic : tidx.keys()) {
                out.println(getProbability((String) e, topic));
            }
            out.println();
        }
    }
}