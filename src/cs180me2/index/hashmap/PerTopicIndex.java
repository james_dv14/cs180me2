package cs180me2.index.hashmap;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T> Type of value stored per topic.
 * @studentno 2012-78002
 */
public abstract class PerTopicIndex<T> extends HashMapIndex<String, T[]> implements PerTopicInterface {
    //***************************************
    // Index dependencies
    //***************************************
    protected static TopicIndex tidx;
    //***************************************
    // Constructor
    //***************************************
    protected PerTopicIndex() throws IndexDependencyException {
        super();
        if (TopicIndex.isNotInstantiated())
            throw new IndexDependencyException();
        else
            tidx = TopicIndex.getInstance();
    }
    //***************************************
    // Accessor - read
    //***************************************
    public T get(String key, String topic) {
        return ((T[])index.get(key))[tidx.getIndex(topic)];
    }
    //***************************************
    // Accessor - write
    //***************************************
    public void set(String key, String topic, T value) {
        ((T[])index.get(key))[tidx.getIndex(topic)] = value;
    }
}