package cs180me2.index.hashmap;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface PerTopicInterface {
    public void put(String key);
}
