package cs180me2.index.hashmap;

import cs180me2.index.IndexBase.IndexDependencyException;
import cs180me2.util.fileio.CollectionPrinterDecorator;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class InstanceIndex extends PerTopicIndex<Integer> {
    //***************************************
    // Singleton implementation
    //***************************************
    private static InstanceIndex instance;
    
    private InstanceIndex() throws IndexDependencyException {
        super();
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static InstanceIndex getInstance() throws IndexDependencyException {
        if (isNotInstantiated())
            instance = new InstanceIndex();
        return instance;
    } 
    //***************************************
    // Semantic alias for get
    //***************************************
    public int getInstances(String word, String topic) {
        return super.get(word, topic);
    }
    
    public int getTotalInstances(String word) {
        int result = 0;
        for (String topic : tidx.keys())
            result += super.get(word, topic);
        return result;
    }
    //***************************************
    // Put
    //***************************************
    public void put(String key) {
        Integer[] data = new Integer[tidx.size()];
        for (int i = 0; i < data.length; i++)
            data[i] = 0;
        index.put(key, data);
    }
    //***************************************
    // Add
    //***************************************
    public void add(String key, String topic) {
        if (index.containsKey(key)) {
            super.set(key, topic, super.get(key, topic) + 1);
        }
        else {
            put(key);
            super.set(key, topic, 1);
        }
    }
    //***************************************
    // Print
    //***************************************
    public void print(String filepath) {
        super.print(new InstanceIndexPrinter(filepath));
    }
    
    private class InstanceIndexPrinter extends CollectionPrinterDecorator {
        //***************************************
        // Constructor
        //***************************************
        public InstanceIndexPrinter(String filepath) {
            super(filepath, keys());
        }
        //***************************************
        // Decorate abstract method
        //***************************************
        @Override
        public void printElement(Object e) {
            out.print(e + "|");
            for (String topic : tidx.keys()) {
                int instances = getInstances((String)e, topic);
                out.print(instances + "|");
            }
            out.println();
        }
    }
}