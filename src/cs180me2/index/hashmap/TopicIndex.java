package cs180me2.index.hashmap;

import cs180me2.util.fileio.CollectionPrinterDecorator;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class TopicIndex extends HashMapIndex<String, Integer[]> {
    //***************************************
    // Constants
    //***************************************
    public static final int FIELDS = 2;
    public static final int IDX_INDEX = 0;
    public static final int IDX_INSTANCES = 1;
    //***************************************
    // Instance fields
    //***************************************
    private int nextIndex;
    //***************************************
    // Singleton implementation
    //***************************************
    private static TopicIndex instance;
    
    private TopicIndex() {
        super();
        nextIndex = 0;
    }
    
    public static boolean isNotInstantiated() {
        return instance == null;
    }
    
    public static TopicIndex getInstance() {
        if (isNotInstantiated())
            instance = new TopicIndex();
        return instance;
    } 
    //***************************************
    // Accessor - write
    //***************************************
    public void put(String topic) {
        if(!index.containsKey(topic)) {
            int[] data = new int[FIELDS];
            data[IDX_INDEX] = nextIndex;
            data[IDX_INSTANCES] = 0;
            index.put(topic, data);
            nextIndex++;
        }
    }
    
    public void add(String topic) {
        ((int[])index.get(topic))[IDX_INSTANCES] += 1;
    }
    //***************************************
    // Accessors - read
    //***************************************
    public int getIndex(String topic) {
        return ((int[])index.get(topic))[IDX_INDEX];
    }
    
    public int getInstances(String topic) {
        return ((int[])index.get(topic))[IDX_INSTANCES];
    }
    //***************************************
    // Print
    //***************************************
    public void print(String filepath) {
        super.print(new TopicIndexPrinter(filepath));
    }
    
    private class TopicIndexPrinter extends CollectionPrinterDecorator {
        //***************************************
        // Constructor
        //***************************************
        public TopicIndexPrinter(String filepath) {
            super(filepath, keys());
        }
        //***************************************
        // Decorate abstract method
        //***************************************
        @Override
        public void printElement(Object e) {
            out.print("[" + getIndex((String) e) + "] ");
            out.print(e + " ");
            out.println(getInstances((String) e));
        }
    }
}