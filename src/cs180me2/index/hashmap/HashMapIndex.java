package cs180me2.index.hashmap;

import cs180me2.index.IndexBase;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author MARIANO, J Stephen DV
 * @param <T1> Type of hash key.
 * @param <T2> Type of hash values.
 * @studentno 2012-78002
 */
public class HashMapIndex<T1, T2> extends IndexBase {
    //***************************************
    // Index
    //***************************************
    protected HashMap index;
    //***************************************
    // Constructor
    //***************************************
    protected HashMapIndex() {
        index = new HashMap<T1, T2>();
    }
    //***************************************
    // HashMap accessors
    //***************************************
    public int size() {
        return index.size();
    }
    
    public Set<T1> keys() {
        return index.keySet();
    }
    
    public boolean containsKey(T1 key) {
        return index.containsKey(key);
    }
    
    public void remove(T1 key) {
        index.remove(key);
    }
    
    public void clear() {
        index.clear();
    }
 }