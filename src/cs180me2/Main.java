/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs180me2;

import cs180me2.index.IndexBase;
import cs180me2.index.array.ConfusionIndex;
import cs180me2.index.hashmap.*;
import cs180me2.index.hashset.*;
import cs180me2.testing.TestingDocScanner;
import cs180me2.training.*;
/**
 *
 * @author JaymNico
 */
public class Main {
    public static final double TRAIN_FACTOR = 0.6;
    public static final int FILE_COUNT = 18446;
    public static final String DATASET_DIR = "files/dataset/";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IndexBase.IndexDependencyException {
        //*******************************************
        // Indexes;
        //*******************************************
        DocumentIndex didx = DocumentIndex.getInstance();
        //*******************************************
        // Scan index
        //*******************************************
        System.out.println("Scanning index file... ");
        IndexScanner dataset_idx = new IndexScanner("files/index");
        dataset_idx.scan();
        //*******************************************
        // Scan wordlists
        //*******************************************
        System.out.println("Scanning dictionary... ");
        DictionaryScanner dict = new DictionaryScanner("files/dictionary");
        dict.scan();
        System.out.println("Scanning stopwords...");
        StopwordScanner stop = new StopwordScanner("files/stopwords");
        stop.scan();
        //*******************************************
        // Prepare for main loop
        //*******************************************
        System.out.println("Training... ");
        int trainingSetSize = (int)(FILE_COUNT * TRAIN_FACTOR);
        int i = 0;
        long trainEnd;
        long testStart = 0;
        long testEnd;
        long trainStart = System.currentTimeMillis();
        //*******************************************
        // Main loop
        //*******************************************
        for(String filename : didx.keys()) {
            //*******************************************
            // Divide between training and testing set
            //*******************************************
            if (i < trainingSetSize) {
                TrainingDocScanner doc = new TrainingDocScanner(DATASET_DIR + filename);
                doc.scan();
                doc.trainAs(filename + "");
            }    
            else {
                TestingDocScanner doc = new TestingDocScanner(DATASET_DIR + filename);
                doc.scan();
                doc.classify(filename + "");
            }
            //*******************************************
            // Calculate some probabilities halfway
            //*******************************************
            if (i == trainingSetSize - 1) {
                trainEnd = System.currentTimeMillis();
                System.out.println("Finished training on " 
                        + (trainingSetSize) + " files in " 
                        + (trainEnd - trainStart) + "ms.");
                System.out.println("Generating lookup tables... ");
                CalculateProbabilities.execute();
                System.out.println("Testing... ");
                testStart = System.currentTimeMillis();
            }
            //*******************************************
            // Loop control
            //*******************************************
            i++;
            if (i > FILE_COUNT)
                break;
        }
        testEnd = System.currentTimeMillis();
        System.out.println("Finished classifying " 
                + (FILE_COUNT - trainingSetSize) + " files in " 
                + (testEnd - testStart) + "ms.");
        System.out.println(ConfusionIndex.getInstance().getAccuracy(FILE_COUNT - trainingSetSize));
        ConfusionIndex.getInstance().print("files/output/confusion");
        TopicIndex.getInstance().print("files/output/topic");
        //PresenceIndex.getInstance().print("files/output/prob");
        //InstanceIndex.getInstance().print("files/output/instance");
    }
}
