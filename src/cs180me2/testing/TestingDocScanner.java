package cs180me2.testing;

import cs180me2.index.IndexBase;
import cs180me2.index.array.*;
import cs180me2.index.hashmap.*;
import cs180me2.math.NaiveBayes;
import cs180me2.util.fileio.LineScannerDecorator;
import cs180me2.util.stem.Stemmer;
import java.util.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class TestingDocScanner extends LineScannerDecorator {
    //***************************************
    // Index dependencies
    //***************************************
    DocumentIndex didx;
    TopicIndex tidx;
    PriorIndex pridx;
    PresenceIndex pidx;
    AbsenceIndex aidx;
    ConfusionIndex cidx;
    //***************************************
    // Collections
    //***************************************
    HashSet<String> wordsInDoc = new HashSet();
    //***************************************
    // Constructor
    //***************************************
    public TestingDocScanner(String filepath) throws IndexBase.IndexDependencyException {
        super(filepath);
        didx = DocumentIndex.getInstance();
        tidx = TopicIndex.getInstance();
        pridx = PriorIndex.getInstance();
        pidx = PresenceIndex.getInstance();
        aidx = AbsenceIndex.getInstance();
        cidx = ConfusionIndex.getInstance();
    }
    //***************************************
    // Decorate abstract method
    //***************************************
    @Override
    public void processLine(String line) {
        String[] words = line.split("[\\s+]");
        for (String word : words) {
            String word_t = Stemmer.getStem(word);
            if (pidx.containsKey(word_t))
                wordsInDoc.add(word_t);
        }
    }    
    //***************************************
    // Main method
    //***************************************
    public void classify(String filename) throws IndexBase.IndexDependencyException {
        HashMap<Double, String> scoreHash = new HashMap();
        PriorityQueue<Double> scoreMaxHeap = new PriorityQueue<>(tidx.size(), Collections.reverseOrder());
        
        for (String topic : tidx.keys()) {
            double score = NaiveBayes.getCPLog(topic, wordsInDoc); 
            scoreMaxHeap.add(score);
            scoreHash.put(score, topic);
        }
        // System.out.println(filename + "(" + didx.getTopic(filename) + ") classified as " + scoreHash.get(scoreHeap.poll()));
        //*******************************************
        // Add topic w/ max score to confusion index
        //*******************************************
        cidx.put(didx.getTopic(filename), scoreHash.get(scoreMaxHeap.poll()));
    }
}