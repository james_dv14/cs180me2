package cs180me2.training;

import cs180me2.util.fileio.LineScannerDecorator;
import cs180me2.index.hashmap.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class IndexScanner extends LineScannerDecorator {
    //***************************************
    // Index dependencies
    //***************************************
    private final DocumentIndex didx;
    private final TopicIndex tidx;
    //***************************************
    // Constructor
    //***************************************
    public IndexScanner(String filepath) {
        super(filepath);
        didx = DocumentIndex.getInstance();
        tidx = TopicIndex.getInstance();
    }
    //***************************************
    // Decorate abstract method
    //***************************************
    @Override
    public void processLine(String line) {
        String[] entry = line.split("\\s+", 2);
        didx.put(entry[0], entry[1]);
        tidx.put(entry[1]);
    }
}
