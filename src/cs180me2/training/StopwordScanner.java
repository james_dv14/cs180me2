package cs180me2.training;

import cs180me2.util.fileio.LineScannerDecorator;
import cs180me2.index.hashset.Stopwords;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class StopwordScanner extends LineScannerDecorator {
    //***************************************
    // Index dependencies
    //***************************************
    private final Stopwords stop;
    //***************************************
    // Constructor
    //***************************************
    public StopwordScanner(String filepath) {
        super(filepath);
        stop = Stopwords.getInstance();
    }
    //***************************************
    // Decorate abstract method
    //***************************************
    @Override
    public void processLine(String line) {
        stop.add(line);
    }
}