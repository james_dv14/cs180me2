package cs180me2.training;

import cs180me2.Main;
import cs180me2.index.IndexBase.IndexDependencyException;
import cs180me2.index.hashmap.*;
import cs180me2.index.array.PriorIndex;
import cs180me2.math.Prob;


/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CalculateProbabilities {
    //***************************************
    // Index dependencies
    //***************************************
    static DocumentIndex didx;
    static TopicIndex tidx;
    static InstanceIndex iidx;
    static PresenceIndex pidx;
    static PriorIndex pridx;
    //***************************************
    // Main
    //***************************************
    public static void execute() throws IndexDependencyException {
        tidx = TopicIndex.getInstance();
        
        calculatePrior();
        calculatePresence();
        /*
        System.err.println("be " + iidx.containsKey("be"));
        System.err.println("in " + iidx.containsKey("in"));
        System.err.println("of " + iidx.containsKey("of"));
        System.err.println("on " + iidx.containsKey("on"));
        System.err.println("are " + iidx.containsKey("are"));
        */
    }
    //***************************************
    // Prior
    //***************************************
    private static void calculatePrior() throws IndexDependencyException {
        didx = DocumentIndex.getInstance();
        pridx = PriorIndex.getInstance();
        
        for (String topic : tidx.keys()) {
            double prior = Prob.getPPLog(tidx.getInstances(topic),
                    (int)(didx.size() * Main.TRAIN_FACTOR));
            pridx.set(topic, prior);
        }
    }
    //***************************************
    // Presence
    //***************************************
    private static void calculatePresence() throws IndexDependencyException {
        iidx = InstanceIndex.getInstance();
        pidx = PresenceIndex.getInstance();
        
        for (String key : iidx.keys()) {
            // System.out.println("Calculating probabilities for word " + key + "...");
            int total = 0;
            int small = 0;
            //int big = 0;
            for (String topic : tidx.keys()) {
                total += iidx.getInstances(key, topic);
                if (iidx.getInstances(key, topic) < 1)
                    small++;
                /*
                else if (iidx.getInstances(key, topic) >= 50)
                    big++;
                */
            }
            if (total > 0 && small > 0)
                pidx.put(key);
        }
    }
}
