package cs180me2.training;

import cs180me2.index.IndexBase.IndexDependencyException;
import cs180me2.index.hashmap.*;
import cs180me2.index.hashset.Dictionary;
import cs180me2.util.fileio.LineScannerDecorator;
import cs180me2.util.stem.Stemmer;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class TrainingDocScanner extends LineScannerDecorator {
    //***************************************
    // Index dependencies
    //***************************************
    Dictionary dict;
    DocumentIndex didx;
    TopicIndex tidx;
    InstanceIndex iidx;
    //***************************************
    // Collections
    //***************************************
    HashSet<String> wordsInDoc = new HashSet();
    //***************************************
    // Constructor
    //***************************************
    public TrainingDocScanner(String filepath) throws IndexDependencyException {
        super(filepath);
        dict = Dictionary.getInstance();
        didx = DocumentIndex.getInstance();
        tidx = TopicIndex.getInstance();
        iidx = InstanceIndex.getInstance();
    }
    //***************************************
    // Decorate abstract method
    //***************************************
    @Override
    public void processLine(String line) {
        String[] words = line.split("[\\s+]");
        for (String word : words) {    
            wordsInDoc.add(Stemmer.getStem(word));
        }
    }    
    //***************************************
    // Main method
    //***************************************
    public void trainAs(String filename) {
        tidx.add(didx.getTopic(filename));

        for (String word_t : wordsInDoc) {
            if (dict.contains(word_t)) {
                if (!iidx.containsKey(word_t))
                    iidx.put(word_t);
                iidx.add(word_t, didx.getTopic(filename));
            }
        }
    }
}