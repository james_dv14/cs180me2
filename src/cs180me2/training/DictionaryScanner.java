package cs180me2.training;

import cs180me2.index.hashset.Dictionary;
import cs180me2.util.fileio.LineScannerDecorator;
import cs180me2.util.stem.Stemmer;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class DictionaryScanner extends LineScannerDecorator {
    //***************************************
    // Index dependencies
    //***************************************
    Dictionary dict;
    //***************************************
    // Constructor
    //***************************************
    public DictionaryScanner(String filepath) {
        super(filepath);
        dict = Dictionary.getInstance();
    }
    //***************************************
    // Decorate abstract method
    //***************************************
    @Override
    public void processLine(String line) {
        dict.add(Stemmer.getStem(line));
    }
}